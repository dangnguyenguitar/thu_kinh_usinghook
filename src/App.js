import logo from "./logo.svg";
import "./App.css";
import Glasses_Tryon from "./Glasses_Tryon/Glasses_Tryon";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Homepage from "./Homepage/Homepage";
import Header from "./utils/Header";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route
            path="/thu-kinh"
            render={() => {
              return <Glasses_Tryon />;
            }}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
