import React, { Component } from "react";
import ChoosingList from "./ChoosingList";
import RenderModelFace from "./RenderModelFace";
import WelcomeBanner from "./WelcomeBanner";

export default class Glasses_Tryon extends Component {
  state = {
    imgSrc: "./glassesImage/v1.png",
  };
  handleChangeGlasses = (newGlass) => {
    this.setState({
      imgSrc: `./glassesImage/${newGlass}`,
    });
  };
  render() {
    return (
      <div className="container pt-5">
        <WelcomeBanner />
        <RenderModelFace currentGlass={this.state.imgSrc} />
        <ChoosingList handleChangeGlasses={this.handleChangeGlasses} />
      </div>
    );
  }
}
