import React, { Component } from "react";

export default class ChoosingList extends Component {
  render() {
    return (
      <div>
        <div className="glasses-container row p-5">
          <button
            onClick={() => {
              this.props.handleChangeGlasses(this.props.dataGlasses.url);
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g1.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v2.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g2.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v3.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g3.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v4.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g4.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v5.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g5.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v6.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g6.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v7.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g7.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v8.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g8.jpg"
              alt=""
            />
          </button>
          <button
            onClick={() => {
              this.props.handleChangeGlasses("v9.png");
            }}
            style={{ width: "20px", height: "80px" }}
            className="col-2 p-1 bg-white m-3"
          >
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src="./glassesImage/g9.jpg"
              alt=""
            />
          </button>
        </div>
      </div>
    );
  }
}
