import React, { Component } from "react";

export default class RenderModelFace extends Component {
  render() {
    return (
      <div>
        <div className="img-container d-flex justify-content-center">
          <div className="modelWithGlasses position-relative">
            <img
              style={{ width: "300px", height: "100%" }}
              src="./glassesImage/model.jpg"
              alt=""
              className="rounded"
            />
            <img
              style={{
                position: "absolute",
                top: "25%",
                left: "0",
                right: "0",
                marginLeft: "auto",
                marginRight: "auto",
                width: "170px",
                opacity: "60%",
              }}
              src={this.props.currentGlass}
              alt=""
            />
          </div>
          <img
            style={{ width: "300px", marginLeft: "100px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
        </div>
      </div>
    );
  }
}
