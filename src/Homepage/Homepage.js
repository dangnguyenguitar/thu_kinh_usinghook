import React, { Component } from "react";
import Header from "../utils/Header";

export default class Homepage extends Component {
  render() {
    return (
      <div className="p-3 mb-2 bg-dark text-white">
        <h2>Home</h2>
      </div>
    );
  }
}
