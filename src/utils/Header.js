import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <div className="container text-left py-3">
        <button className="btn btn-warning mr-5">
          <NavLink to={"/"}>Home</NavLink>
        </button>
        <button className="btn btn-warning">
          <NavLink to={"/thu-kinh"}>Thử kính</NavLink>
        </button>
      </div>
    );
  }
}
